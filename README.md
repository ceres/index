# Hobby coding projects 
<p>  This is an index of some of the hobby coding projects I have done outside of my work. The motiviation behind these projects is strictly educational in nature, the curiosity to understand how things work under hood, how various components interact with each other, interface between hardware and software and such.</p>
  <p> All these projects are result of my own research and reading various sources of information including wading through source code repositories of open source projects.</p>
 <p>This learning process although at times frustrating has also been highly rewarding and has helped me to understand few things better than I otherwise would have.</p>
 <p> All these projects are written in C, although I am no expert C programmer, I like the language for its simplicity and power<p>
 <p> At the same time I am aware of all the potential pitfalls it can cause that only a seasoned C programmer can foresee and avoid, hopefully I'll get there someday<p>

## An UNIX daemon, 
<p>It does nothing useful but helped me understand how a process gets daemonized in UNIX, detaching itself from the controlling TTY, closing its STDIN,STDOUT,STDERR, forking twice to become the session leader and process group leader.<p>

* [Source](https://gitlab.com/ceres/daemon.git)

## An UNIX shell
<p> A primitive shell written in C, which currently has very basic functionality, any unix command can be executed inside it, has no memory leaks and obvious errors. <p>
<p>This is an ongoing project, in the next phase I intend to add pipe support and redirection <p>
<p> I call this a losh <p>

* [Source](https://gitlab.com/ceres/losh.git)

## An utility to read raw network packets
<p> It utilizes libpcap library to gain access to RAW packets, same as tcpdump <p>
<p> Currently, it prints all the packet header data from layer 2,3 and 4 <p>

* [Source](https://gitlab.com/ceres/dot.git)

## An utility to read raw disk blocks
<p> This utility opens the disk in binary mode reads raw sectors into memory, it then parses and dumps the content in hexadecimal <p>
<p>I wrote this to understand GPT partitioning data and EXT4 superblock as they are laid out on the disk <p>

* [Source](https://gitlab.com/ceres/fs.git)
